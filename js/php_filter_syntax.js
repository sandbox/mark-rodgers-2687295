var editor;

function check_syntax($format) {
	if($format == "php_code") {
		myTextarea = document.getElementById("edit-body-und-0-value");
		if(!editor) {
			editor = CodeMirror.fromTextArea(myTextarea, {
				lineNumbers: true,
				matchBrackets: true,
				mode: "php"
			});
		}
	} else {
		if(editor) {
			editor.toTextArea();
			editor = null;
		}
	}
}

document.addEventListener("DOMContentLoaded", function(event) { 
	var filterlist = document.getElementsByClassName("filter-list")[0];
	filterlist.setAttribute("onchange", "check_syntax(this.value)");
	check_syntax(filterlist.value);
});
